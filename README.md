## External sorting
The input of the progam is an unsorted binary uint32_t file what is supposed to be large (~4GB). The input file is generated in main.cpp. 

### Pre-sorting
At pre-sorting the program divides the file to n parts. It executes sorting by a standard sorting algorithm, and writes the results into a buffer file. After pre-sorting 
the buffer file consist of n independent and sorted chunk.

### Merging
In the next step, the program merges the sorted chunks. If we select the first element of each chunk, we can be sure that these are the smallest elements of chunks thus
they are sorted. If we choose the smallest element of these selected elements, we can be sure that, it is the smallest in the whole file. 

 * chunkBufferSize = [parameter of the class - maximum amount of elements in buffer]
 * numberOfChunks = ceil(sizeOfFile/chunkBufferSize)
 * sizeOfSlice = chunkBufferSize/(numberOfChunks+1) [1 extra for the output buffer]
 * all buffers together = chunkBufferSize

In the merging step it fills n buffers (sizeOfSlice size) with elements of the front of each chunk. It selects the smallest element of the front of buffer vectors, and put in a buffer. After 
selecting, it removes the element from the buffer. (calling erase method of vector at each selection would be very resource consuming, so it is executed after only once in a sequence).
When the output buffer is full, it writes that in the file. 

![alt text](img/nway.png)

## Building code
In order to build the code, open Debug folder and run *make*.

## Running code
```'c++
 ./Release/external_sorting 10000000074 24009001
```
 * [arg1]: number of elements in input file, size = arg1 * 4 (4 byte integers)
 * [arg2]: size of buffer (number of integers) = buffer size in bytes / sizeof(uint32_t)


## Results
After running the code with default settings, you are supposed to get something similar:
```'c++
file not found
Generating data 
sizeOfFile: 4000000296 chunkBufferSize: 96036004 numberOfChunks: 42
time: 4201.83 sec
file is sorted
DONE
```
