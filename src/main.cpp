//============================================================================
// Name        : main.cpp
// Author      : A. Door
// Version     :
// Copyright   : Your copyright notice
// Description :
//============================================================================

#include <iostream>
#include <fstream>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include "ExternalSorting.h"
#define INPUT_FILE_NAME "unsorted_numbers.bin"
#define BUFFER_FILE_NAME "buffer_file.bin"
#define OUTPUT_FILE_NAME "sorted_numbers.bin"
using namespace std;



bool isFileSorted(string File)
{
	uint32_t num_prev = 0;
	ifstream file;
	uint8_t b[4];
	//is file empty
	ifstream f(OUTPUT_FILE_NAME, ifstream::ate | ifstream::binary); //opening file, and replacing the pointer to EOF
	if(f.tellg() == 0)
	{
		cout << "file is empty" << endl;
		f.close();
		return -1;
	}
	f.close();

	while(true)
	{
		file.read((char*)b,4);
		if( file.eof() ) break;

		uint32_t num_act = (b[3] << 24) | (b[2] << 16) | (b[1] << 8) | (b[0]);
		if(num_prev > num_act)
		{
			cout << "file is unsorted" << "at address: " << file.tellg() << endl;
			file.close();
			return false;
		}
		num_prev = num_act;

	}
	file.close();
	cout << "file is sorted" << endl;
	return true;
}


// TODO using arguments for MAX_BUFFER_SIZE, and NUM_OF_NUMS
int main(int argc, char *argv[]) {
	if(argc < 3)
	{
		cout << "Too few arguments arg1: number of data, arg2: number of integers of buffer" << endl;
		return -1;
	}

	//generating data
	ifstream f(INPUT_FILE_NAME);
	if(!f.good())
	{

		f.close();
		cout << "file not found" << endl;
		cout << "Generating data "<< endl;
		ofstream f;
		f.open(INPUT_FILE_NAME, ofstream::out | ofstream::binary);
		for(uint32_t i=0; i< (unsigned int)atoi(argv[1]); i++)
		{
			uint32_t number = rand();
			f.write(reinterpret_cast<const char *>(&number), sizeof(number));
		}
		f.close();
	}
	else
	{
		cout << "file found" << endl;

	}

	const clock_t begin_time = clock();

	ExternalSorting srt(INPUT_FILE_NAME,OUTPUT_FILE_NAME, BUFFER_FILE_NAME, atoi(argv[2]));
	srt.Sorting();

	cout << "time: " << float( clock () - begin_time ) /  CLOCKS_PER_SEC << " sec" << endl;
	isFileSorted(OUTPUT_FILE_NAME);
	cout << "DONE" << endl;
	return 0;
}
