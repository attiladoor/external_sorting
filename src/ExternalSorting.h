/*
 * ExternalSorting.h
 *
 *  Created on: Jun 5, 2017
 *      Author: attila
 */

#ifndef EXTERNAL_SORTING_H_
#define EXTERNAL_SORTING_H_

#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>

using namespace std;

class ExternalSorting {


public:
	ExternalSorting(string INPUT_FILE, string OUTPUT_FILE, string BUFFER_FILE_NAME, unsigned long bufferSizeInput);
	~ExternalSorting();
	void Sorting();
	void printBuffer();
private:
	string inputFile;
	string outputFile;
	string bufferFile;

	unsigned int sizeOfFile;
	unsigned int chunkBufferSize;
	int numberOfChunks;

	void loadingDataToBuffer(string file, int chunk, unsigned long chunkSize, unsigned long offset,unsigned long size, vector<uint32_t>* v);
	void getSortingParameters();
	void firstSorting();
	void nWayMerging();
	bool isMatrixEmpty(vector< vector<uint32_t> >* v);
	void writingFile(ofstream* fo, vector<uint32_t> v, unsigned long address);
};

#endif /* EXTERNAL_SORTING_H_ */
