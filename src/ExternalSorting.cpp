/**
	ExternalSorting.cpp
 	 Purpose: Performs a sorting algorithm on large file
	Created on: Jun 5, 2017
 	 @author: A. Door
 */

#include "ExternalSorting.h"

using namespace std;

/**
 * Constructor of ExternalSorting class
 * @INPUT_FILE: path of input file, where is stored the data for sorting
 * @OUTPUT_FILE: path of output file, where we want to store our sorted data
 * @BUFFER_FILE_NAME: temporary file for "firstSorting: function
 * @bufferSizeInput: number of buffer in number of integers, so final size = bufferSizeInput*sizeof(uint32_t)
 */


ExternalSorting::ExternalSorting(string INPUT_FILE, string OUTPUT_FILE, string BUFFER_FILE_NAME, unsigned long bufferSizeInput)
{
	inputFile = INPUT_FILE;
	outputFile = OUTPUT_FILE;
	bufferFile = BUFFER_FILE_NAME;
	// uploading variables: sizeOfFile, bufferSize, numberOfChunks
	ifstream f(inputFile.c_str(), ifstream::ate | ifstream::binary); //opening file, and replacing the pointer to EOF
	sizeOfFile = f.tellg(); //getting the position of file pointer

	//finding an optimal size of chunkBuffer
	if(bufferSizeInput*sizeof(uint32_t) > sizeOfFile)
		chunkBufferSize = sizeOfFile;
	else // in case you add a number which is cannot divided by 4 without 0 modulo
	{
		chunkBufferSize = bufferSizeInput*sizeof(uint32_t);
		chunkBufferSize -= chunkBufferSize%sizeof(uint32_t);
	}

	numberOfChunks = sizeOfFile/chunkBufferSize;
	if(sizeOfFile%chunkBufferSize != 0)
		numberOfChunks++;

	cout << "sizeOfFile: " << sizeOfFile << " chunkBufferSize: " << chunkBufferSize << " numberOfChunks: " << numberOfChunks << endl;
	f.close();
}

/**
 * Destructor of ExternalSorting class
 */

ExternalSorting::~ExternalSorting() {
}
/*
 * firstSorting:
 * Pre-sorting step of algorithm, we separate the file to "numberOfChunk" chunk and execute sorting on each chunk
 *
 */

void ExternalSorting::firstSorting()
{
	vector<uint32_t> v;
	ofstream fo(bufferFile.c_str(), ofstream::out | ofstream::binary);

	for(int chunk=0; chunk<numberOfChunks; chunk++)
	{
		v.clear();
		//reading one chunk of data
		loadingDataToBuffer(inputFile, chunk, chunkBufferSize, 0 , chunkBufferSize, &v);
		sort(v.begin(),v.end());
		writingFile(&fo, v, chunkBufferSize*chunk);
	}

	fo.close();
	v.clear();
}

/*
 * loadingDataToBuffer:
 * @file: input file name
 * @chunkSize: size of Chunks, it expresses the blocks of data, and it serves also as a limit
 * @offset: address offset in chunk, in case we would like to read not the first element of chunk
 * @size: size of data we would like to read
 * @v: vector pointer, where we would store data
 *
 * !WARNING: it cleans vector
 */

void ExternalSorting::loadingDataToBuffer(string file, int chunk, unsigned long chunkSize, unsigned long offset,unsigned long size, vector<uint32_t>* v)
{

	ifstream f(file.c_str(), ifstream::in | ifstream::ate | ifstream::binary);

	//address of the next chunk
	f.seekg(((unsigned long)chunk+1)*chunkSize);
	unsigned long limit = f.tellg();

	//find address
	f.seekg((unsigned long)chunk*chunkSize + offset);

    v->clear();
    uint32_t value;
    while(v->size()*sizeof(uint32_t) < size)
    {

    	f.read((char*)&value,sizeof(uint32_t));
    	if(f.eof())
    		break;
    	if((unsigned long)f.tellg() > limit)
    		break;
    	v->push_back(value);
    }

    f.close();

}

/*
 * nWayMerging:
 * Second stage of external sorting
 * 1, It allocates chunkNumber+1 buffer vector with size of bufferSize together. It fills "chunkNumber" of buffers with elements from the
 * chunk belonging to them.  Each buffer is sorted because it reads sorted data from the chunk.
 * 2, Each step, it iterates over the chunk buffer and chooses the smallest element, and then put it in the last buffer, which is the output buffer
 * 	  Would have quite huge overhead if it resizes each buffer in each rounds so better to save the selected number of datas regarding to each buffer
 * 	  and resize the buffer at the end of the sequence. It also stores the sum of selected numbers regarding to each buffer, because we can use
 * 	  as an offset at reading data from bufferFile.
 * 3, Erasing selected elements from buffers
 * 4, Writing content into file
 * 5, Repeating as long as all the buffers are not empty
 *
 */


void ExternalSorting::nWayMerging()
{
	unsigned long sizeOfSlice = chunkBufferSize/(numberOfChunks+1);
	sizeOfSlice -= sizeOfSlice%sizeof(uint32_t);

	vector< vector<uint32_t> >v(numberOfChunks+1, vector<uint32_t>());
	unsigned long numberOfSelectedGeneral[numberOfChunks];
	fill_n(numberOfSelectedGeneral, numberOfChunks, 0);
	unsigned long numberOfSelected[numberOfChunks];
	fill_n(numberOfSelected, numberOfChunks, 0);
	int sequence = 0;

	ofstream fo(outputFile.c_str(), ofstream::out | ofstream::binary);
	while(true)
	{	//filling buffer
		for(int chunk = 0; chunk < numberOfChunks; chunk++)
		{
			loadingDataToBuffer(bufferFile, chunk, chunkBufferSize, numberOfSelectedGeneral[chunk]*sizeof(uint32_t), sizeOfSlice, &v[chunk]);
		}

		//reset container
		fill_n(numberOfSelected, numberOfChunks, 0);
		//checking is all the buffer empty -> break
		bool isEmpty = true;
		for(int i =0; i< v.size(); i++ )
		{
			if(v[i].size() > 0)
			{
				isEmpty = false;
				break;
			}
		}
		if(isEmpty)
			break;

		//clearing output buffer
		v[numberOfChunks].clear();
		//merging
		for(unsigned long i = 0; i < sizeOfSlice/sizeof(uint32_t); i++)
		{
			uint32_t minValue = UINT32_MAX;
			int minChunk = -1;
			//choosing min element from buffers
			for(int chunk = 0; chunk < numberOfChunks; chunk++)
			{
				if(numberOfSelected[chunk] < v[chunk].size()) //we havn't ran out from the vector
				{
					if(v[chunk][numberOfSelected[chunk]] < minValue)
					{
						minValue = v[chunk][numberOfSelected[chunk]];
						minChunk = chunk;
					}
				}
			}
			//we found a smaller number
			if(minChunk != -1)
			{
				v[numberOfChunks].push_back(minValue);
				numberOfSelectedGeneral[minChunk]++;
				numberOfSelected[minChunk]++;
			}
		}
		//removing selected values from buffer
		for(int chunk = 0; chunk < numberOfChunks; chunk++)
		{
			v[chunk].erase(v[chunk].begin(), v[chunk].begin()+numberOfSelected[chunk]);
		}

		//clearing numberOfSelected buffer
		fill_n(numberOfSelected, numberOfChunks, 0);

		//writing into file
		writingFile(&fo, v[numberOfChunks], sequence*sizeOfSlice);
		sequence++;

	}
	fo.close();

}

/*
 * writingFile:
 * @fo: file pointer of our target file
 * @v: vector which's content we would like to write in file
 * @address: address in the file (offset), where we would like to write data
 */

void ExternalSorting::writingFile(ofstream* fo, vector<uint32_t> v, unsigned long address)
{
	fo->seekp(address);
	for(unsigned long i=0; i < v.size(); ++i)
	{
		if(v.size())
			fo->write((char*)& v[i],4);
	}
}

/*
 * Sorting:
 * Main function of ExternalSorting method, it calls the sub functions.
 */
void ExternalSorting::Sorting()
{
	firstSorting();
	nWayMerging();
}

